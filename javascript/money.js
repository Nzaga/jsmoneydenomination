//object of money denomination
var denomination = {};

//function to receive the money
var receive = function (amount){
	 if ( typeof amount === 'string'){							//identify the type of entered amount
	 	if ( isNaN(amount)){									//check if the entered value is a number
			console.log("You entered not a number");
		}
		else{
			checkForDecimal(amount);							//check for decimals
		}
	 }
	 else{
	 	var enteredAmount = amount.toString();
	 	checkForDecimal(enteredAmount);
	 }
};

function checkForDecimal (amount){
	var decimalPlacePosition = amount.indexOf(".");					//get the psotion of the decimal place
	if ( decimalPlacePosition != -1){
		var amountToString = amount.toString().split(".");			//split the amount into currency and cents
		denomination.cents = amountToString[1];						//add cents property to the denomination
		denomination.total = Number (amountToString[0]);			//add the total currency on the denomination object
		findTheDenomination();
	}
	else{
		denomination.cents = 0;						//add cents property to the denomination
		denomination.total = Number(amount);
		findTheDenomination();
	}
}

function findTheDenomination (){

	var total = denomination.total;
	var divider = 10000;
	var note = [];
	var noteCount;
	var balance = total;

	while( balance !== 0){
		if ( divider == 10000){
			denaminationCount();
			divider /= 2;
		}
		else if ( divider == 5000){
			denaminationCount();
			divider = 2000;
		}
		else if ( (divider < 5000) && ( divider > 500)){
			denaminationCount();
			divider /= 2;
		}
		else if ( divider == 500){
			denaminationCount();
			divider = 200;

		}
		else if ( (divider <= 200) && ( divider > 50 )){
			denaminationCount();
			divider /=2 ;

		}
		else if ( divider == 50){
			denaminationCount();
			divider = 20 ;

		}
		else if ( (divider <=20) && (divider > 5)){
			denaminationCount();
			divider = 5;

		}
		else if ( divider == 5){
			denaminationCount();
			divider = 1;
		}
		else {
			denaminationCount();
		}

	}

	function denaminationCount (){
		note = (balance/divider).toString().split(".");
		noteCount = note[0];
		balance = balance - ( divider * noteCount );
		denomination[divider] = noteCount;
		denomination.balance = balance;
	}
	console.log(denomination);
}





receive(105001.0012);												//enter number or valid number string
